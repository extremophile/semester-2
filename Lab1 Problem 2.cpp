// problem2.100.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <iostream>
#include <vector>
#include <cmath>
#include <time.h>
#include <cstdio>

using namespace std;

vector<int> trivial(int a)
{
	vector<int> result;
	result.push_back(2);
	bool isPrime = true;
	for (int i = 3; i < a; i++)
	{
		for (int j = 2; j < i; j++)
		{
			if (i % j == 0)
			{
				isPrime = false;
				break;
			}
			isPrime = true;
		}
		if (isPrime)
		{
			result.push_back(i);
		}
	}

	return result;
}

vector<int> eratosten(int a)
{
	vector<int> result;
	vector<bool> isPrime(a, true);
	isPrime[0] = isPrime[1] = false;

	for (int i = 2; i < sqrt(a); i++) {
		if (isPrime[i] == true)
		{
			for (int j = i + i; j < a; j += i)
			{
				isPrime[j] = false;
			}
		}
	}
	for (int i = 0; i < isPrime.size(); i++)
	{
		if (isPrime[i] == true)
		{
			result.push_back(i);
		}
	}
	return result;
}

int main()
{
	cout << "Enter a number:" << endl;
	int num;
	cin >> num;
	
	clock_t begin_time = clock();

	vector<int> result = eratosten(num);

	cout << "Eratosten time is " << float(clock() - begin_time) / CLOCKS_PER_SEC << " seconds." << endl;

	cout << endl;

	begin_time = clock();
	
	vector<int> resultdumb = trivial(num);
	
	cout << "Trivial time is " << float(clock() - begin_time) / CLOCKS_PER_SEC << " seconds." << endl;

	return 0;
}