#include <iostream>
#include <string>
#include <fstream>
//the big three
using namespace std;
 
template<typename T>
class Array
{
public:
	Array()
	{
		elements = new T[1];
		size = 0;
	}
	~Array()
	{
		delete[] elements;
	}
 
	Array(int size, T * elements) : size(size)
	{
		this->elements = new T[size]; ///vzima se ot obekta elements i se slaga na heap
		for (int i = 0; i < size; i++)
		{
			this->elements[i] = elements[i]; ///i posle prisvoqvame stoinostite
		}
	}
 
	Array(const Array &right)
	{
		size = right.size;
		elements = new T[right.size];
		for (int i = 0; i < right.size; i++)
		{
			elements[i] = right.elements[i];
		}
	}
 
	int getSize() const ///ne izmenq state-a na obekta
	{
		return size;
	}
 
	T & operator[](int index) /// vrushtame referenciq za da mojes da se promenq i t.n.
	{
		return elements[index];
	}
 
	const T & operator[](int index) const /// vrushtame referenciq za da mojes da se promenq i t.n.
	{                                ///purviq const ozn, che vrushta const, a drugiq ozn, che samata func e const(moje da se izvikva ot const referencii i otbekti)
		return elements[index];
	}
 
	Array operator+(Array& right)
	{
		T * arr = new T[right.size];
		for (int i = 0; i < right.size; i++)
		{
			arr[i] = elements[i] + right.elements[i];
		}
		Array hola(right.size, arr);
		return hola;
	}
 
	Array& operator++() //++a + b = purvo shte se digne a i sled tova + b
	{
		T * arr = new T[size + 1];
		for (int i = 0; i < size; i++)
		{
			arr[i] = elements[i];
		}
		size = size + 1;
		delete[] elements;
		elements = arr;
		elements[size - 1] = 2;
		return *this;
	}
	Array operator++(int unused) // a++ + b = purvo vrushta i sled tova stava ++;
	{
		Array old(size, elements);		// da promenim segashniq i da vurnem minaliq, zashtoto posle se izvurshva se edno ++
		T * arr = new T[size + 1];
		for (size_t i = 0; i < size; i++)
		{
			arr[i] = elements[i];
		}
		size = size + 1;                
		delete[] elements;
		elements = arr;
		elements[size - 1] = 1;
		return old;
	}
	Array& operator=(const Array& right) //referenciq e v lqfo zaradi chainvane na func a=b=c=d primerno;
	{
		if (*this != right) //// * dereferira okazatelq, & vzima adresa na obect // inache tuk izpolzvam funciqta po - dolu
		{
			T * arr = new T[right.size];
			for (int i = 0; i < right.size; i++)
			{
				arr[i] = right.elements[i];
			}
			size = right.size;
			delete[] elements;
			elements = arr;
		}
		return *this;
	}
	bool operator!= (const Array& right)
	{
		if (this->size == right.size && this->elements == right.elements)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
 
private:
	T * elements;
	int size;
};
 
template<typename T>
ostream& operator<<(ostream &os, const Array<T>& arr)
{
	os << "Those are the numbers: " << endl;
	for (int i = 0; i < arr.getSize(); i++)
	{
		if (i == arr.getSize() - 1)
		{
			os << arr[i] << endl;
		}
		else
		{
			os << arr[i] << " ";
		}
	}
	return os;
}
 
int main()
{
	int vals[4] = { 1, 2, 3, 4 }; ///vzima pointer kum purviq element ot bloka i sled tova si polzva bloka shtoto pametta tam e zapazena
	Array<int> a1(4, vals);
	Array<int> a2(a1);
 
	cout << a2 << endl;
	cout << a1 + a2 << endl;
	++a1;
	cout << a1 << endl;
	//cout << ++a1 << endl;
	//cout << a1++ << endl;
	a1++;
	cout << "The now changed postfix form: " << a1 << endl;
	a2 = a1;
	cout << a2 << endl;
	return 0;
}
