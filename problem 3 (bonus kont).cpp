// problem 3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <cmath>
#include <stdexcept>
#include <fstream>
#include <sstream>
using namespace std;

template<typename T>
class Vec3
{
public:
	Vec3()
	{
		components = new T[3];
		size = 3;
	}
	Vec3(const Vec3<T> &other) : Vec3()
	{
		for (int i = 0; i < other.size; i++)
		{
			components[i] = other.components[i];
		}
	}
	Vec3<T>& operator=(const Vec3<T> &right)
	{
		if (this != &right)
		{

			T * temp = new T[right.size];
			for (int i = 0; i < right.size; i++)
			{
				temp[i] = right.components[i];
			}
			delete[] components;
			components = temp;
			size = right.size;
		}
		return *this;
	}

	~Vec3()
	{
		delete[] components;
	}

	Vec3<T> operator+(Vec3<T> &right)
	{
		Vec3<T> result;
		for (int i = 0; i < size; i++)
		{
			result[i] = components[i] + right.components[i];
		}
		return result;
	}
	Vec3<T> operator+(T scalar)
	{
		Vec3<T> result;
		for (int i = 0; i < size; i++)
		{
			result[i] = components[i] + scalar;
		}
		return result;
	}

	T operator*(Vec3<T> &right)
	{
		double result = 0;
		for (int i = 0; i < size; i++)
		{
			result += components[i] * right.components[i];
		}
		return result;
	}
	Vec3<T> operator*(T scalar)
	{
		Vec3<T> result;
		for (int i = 0; i < size; i++)
		{
			result[i] = components[i] * scalar;
		}
		return result;
	}

	T& operator[](unsigned int index)
	{
		return components[index];
	}
	T& operator[](unsigned int index) const
	{
		return components[index];
	}

	int get_size() const
	{
		return size;
	}

	double length() const
	{
		double result = 0;
		for (int i = 0; i < size; i++)
		{
			result += pow(components[i], 2);
		}
		return sqrt(result);
	}
private:
	T *components;
	unsigned int size;
};

template<typename T>
ostream& operator<<(ostream& os, const Vec3<T>& v)
{
	os << "(";
	for (int i = 0; i < v.get_size(); i++) //
	{
		if (i == v.get_size() - 1)
		{
			os << v[i] << ")";
		}
		else
		{
			os << v[i] << ", ";
		}
	}
	return os;
}

template<typename T>
istream& operator>>(istream& is, const Vec3<T>& v) // ���� ������� ���� �� 3 ����� ����� ��� � ���� ������ ������ �� �������� ���� � ���� ������
												   // �� ������ ������� � stringstream �� ss �� �������� �������� � �� �� ������� � ������ �� ������
{
	string line;
	getline(is, line);
	stringstream sStream(line);

	for (int i = 0; i < v.get_size(); i++)
	{
		sStream >> v[i];
	}
	return is;
}

int main(int argc, char* argv[])
{
	vector< Vec3<float> > vs;
	Vec3<float> temp;
	ifstream input("3.in");
	while (input >> temp)
	{
		vs.push_back(Vec3<float>(temp));
	}

	for (int i = 0; i < vs.size(); i++)
		cout << vs[i] * 2 << " ";
	cout << endl;

	for (int i = 0; i < vs.size(); i++)
	{
		vs[i] = vs[i] + 1;
		cout << vs[i] << "=" << vs[i].length() << " ";
	}
	cout << endl;

	for (int i = 0; i < vs.size() - 1; i++)
	{
		for (int j = i + 1; j < vs.size(); j++)
		{
			cout << vs[i] + vs[j] << " " << vs[i] * vs[j] << endl;
		}
	}

	return 0;
}

