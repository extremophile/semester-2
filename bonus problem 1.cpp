// problem 1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <map>

using namespace std;

long long recursive(int n)
{
	static map<int, long long> memo; // purvoto e key i zatova e int
	if (n <= 2)
	{
		return 1;
	}
	else
	{
		if (memo.count(n) > 0)
		{
			return memo[n];
		}
		long long ret = recursive(n - 1) + recursive(n - 2) * 2;
		memo[n] = ret;
		return ret;
	}
}

int main()
{
	int n;
	while (cin >> n)
	{
		if (n < 1 || n > 40)
		{
			cout << "Nope. Can't do that pal'." << endl;
			continue;
		}
		cout << recursive(n) << endl;
	}
    return 0;
}

