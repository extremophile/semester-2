// fibonacci memo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <map>
using namespace std;

int fib(int n)
{
	static map<int, int> memo;
	if (n < 2)
	{
		return 1;
	}
	if (memo.count(n) > 0)
	{
		return memo[n];
	}
	int ret = fib(n - 1) + fib(n - 2);
	memo[n] = ret;
	return ret;
}

int main()
{
	int n;
	cin >> n;
	cout << fib(n) << endl;
    return 0;
}

