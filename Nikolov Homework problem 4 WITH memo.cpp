// problem 4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <vector>
#include <map>
/***
FN:F79454
PID:4
GID:1
*/



using namespace std;

long long iterative(int n)
{

	if (n < 4)
	{
		return 1;
	}
	long long f1 = 1;
	long long f2 = 1;
	long long f3 = 1;
	long long sum;
	for (size_t i = 3; i < n; i++)
	{
		sum = 3 * f3 - 2 * f2 + f1;
		f1 = f2;
		f2 = f3;
		f3 = sum;
	}
	return sum;
}

long long recursive(int n)
{
	static map<long long, long long> memo;
	if (n < 4)
	{
		return 1;
	}
	else
	{
		if (memo.count(n) > 0)
		{
			return memo[n];
		}
		long long ret = 3 * recursive(n - 1) - 2 * recursive(n - 2) + recursive(n - 3);
		memo[n] = ret;
		return ret;
	}
}

int main()
{
	int n;
	cin >> n;

	cout << "Recursive: " << recursive(n) << endl;
	cout << "Iterative: " << iterative(n) << endl;
	return 0;
}