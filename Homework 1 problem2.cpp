/***
FN:F79454
PID:2
GID:3
*/
#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>

using namespace std;

const double PI = 3.141592653589793238463;

class Shape
{
public:
	virtual void printArea() const = 0;
	virtual void printPerimeter() const = 0;
	virtual ~Shape() {}
};

class Rectangle : public Shape
{
public:
	Rectangle(double a_, double b_) : a(a_), b(b_)
	{
	}

	virtual void printArea() const
	{
		cout << a*b << endl;
	}

	virtual void printPerimeter() const
	{
		cout << 2 * a + 2 * b << endl;
	}

protected:
	double a, b;
};

class Circle : public Shape
{
public:
	Circle(double r_) : r(r_)
	{
	}

	virtual void printArea() const
	{
		cout << pow(r, 2)*PI << endl;
	}
	virtual void printPerimeter() const
	{
		cout << 2 * PI * r << endl;
	}


protected:
	double r;
};

int main()
{
	ifstream input("2.in");

	int n;
	input >> n;
	vector<Shape *> v;
	for (int i = 0; i < n; i++)
	{
		char type;
		input >> type;
		if (type == 'r')
		{
			double a, b;
			input >> a >> b;
			Rectangle * p = new Rectangle(a, b);
			v.push_back(p);
		}
		else
		{
			double r;
			input >> r;
			Circle * p = new Circle(r);
			v.push_back(p);
		}
	}
	int number;
	char option;
	while (input >> option >> number)
	{
		if (option == 'a')
		{
			v[number - 1]->printArea();
		}
		else
		{
			v[number - 1]->printPerimeter();
		}
	}
	input.close();
	return 0;
}
