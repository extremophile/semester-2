#include <iostream>
#include <vector>

using namespace std;



vector<int> calculate(int a)
{
	vector<int> result;
	while (a != 1)
	{
		if (a % 2 == 0)
		{
			a /= 2;
			result.push_back(2);
			continue;
		}
		if (a % 3 == 0)
		{
			a /= 3;
			result.push_back(3);
			continue;
		}
		if (a % 5 == 0)
		{
			a /= 5;
			result.push_back(5);
			continue;
		}
		if (a % 7 == 0)
		{
			a /= 7;
			result.push_back(7);
			continue;
		}
	}
	return result;
}

int main()
{
	cout << "Enter a number:" << endl;
	int num;
	while (cin >> num)
	{
		vector<int> V = calculate(num);

		cout << num << " = ";

		for (int i = 0; i < V.size(); i++)
		{
			if (i == V.size() - 1)
			{
				cout << V[i] << endl;
				break;
			}
			cout << V[i] << " * ";
		}
	}
	
	return 0;
}
