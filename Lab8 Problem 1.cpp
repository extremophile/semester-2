// problem 1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <set>
#include <string>
using namespace std;

set<int> set_union(set<int>& a, set<int>& b)
{
	set<int> c;
	set<int>::iterator ait;
	for (ait = a.begin(); ait != a.end(); ait++)
	{
		c.insert(*ait);
	}
	set<int>::iterator bit;
	for (bit = b.begin(); bit != b.end(); bit++)
	{
		c.insert(*bit);
	}
	return c;
}

set<int> intersection(set<int>& a, set<int>& b)
{
	set<int> c;
	set<int>::iterator ait;
	set<int>::iterator bit;
	
	for (ait = a.begin(); ait != a.end(); ait++)
	{
		for (bit = b.begin(); bit != b.end(); bit++)
		{
			if (*ait == *bit)
			{
				c.insert(*bit);
			}
		}
	}
	
	return c;
}

int main()
{
	set<int> a;
	set<int> b;

	int n, m;
	cin >> n;
	for (size_t i = 0; i < n; i++)
	{
		int number;
		cin >> number;
		a.insert(number);
	}
	cin >> m;
	for (size_t i = 0; i < m; i++)
	{
		int number2;
		cin >> number2;
		b.insert(number2);
	}
	cout << "Union:" << endl;
	set<int> fin = set_union(a, b);
	set<int>::iterator it;
	for (it = fin.begin(); it != fin.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;

	cout << "Intersection:" << endl;
	set<int> fin2 = intersection(a, b);
	set<int>::iterator it2;
	for (it2 = fin2.begin(); it2 != fin2.end(); it2++)
	{
		cout << *it2 << " ";
	}
	cout << endl;

	return 0;
}


