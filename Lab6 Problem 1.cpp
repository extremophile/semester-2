// problem 1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
using namespace std;

int numberOfCalls = 0;

int fibonaci(int n)
{
	numberOfCalls++;
	if (n <= 2)
	{
		return 1;
	}
	else
	{
		return fibonaci(n - 2) + fibonaci(n - 1);
	}
}

int fibonaciIterative(int n)
{
	int a = 1, b = 1, ith;
	numberOfCalls = 0;
	for (size_t i = 2; i < n; i++)
	{
		numberOfCalls++;
		ith = a + b;
		a = b;
		b = ith;
	}
	return ith;
}

vector<int> mem;
int fibonaciMem(int n)
{
	numberOfCalls++;
	if (mem.size() > n)
	{
		return mem[n];
	}
	if (n <= 2)
	{
		mem.push_back(1);
		return 1;
	}
	else
	{
		int next = fibonaciMem(n - 1) + fibonaciMem(n - 2);
		mem.push_back(next);
		return next;
	}
}

int main()
{
	int number;
	while (cin >> number)
	{
		cout << "recursive: " << fibonaci(number) << endl;
		cout << "recursions count: " << numberOfCalls << endl;

		cout << "recursive(memoization): " << fibonaciMem(number) << endl;
		cout << "recursions(memoization) count: " << numberOfCalls << endl;

		cout << "iterative: " << fibonaciIterative(number) << endl;
		cout << "iterations count: " << numberOfCalls << endl;
	}
}
