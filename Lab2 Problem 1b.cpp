#include <iostream>
 
using namespace std;
 
int main()
{
	int a[] = { 1, 2 ,3 };
	int *pa = a;
 
	for (int i = 0; i < sizeof(a)/sizeof(a[0]); i++) // sizeof(a)/sizeof(a[0])
	{
		if (i == (sizeof(a) / sizeof(a[0])) - 1)
		{
			cout << *(pa + i) << endl;
			break;
		}
		cout << *(pa + i) << " ";
	}
 
    return 0;
}
