// problem2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>

using namespace std;

int factorial(int n)
{
	int temp;

	if (n <= 1)
	{
		return 1;
	}
	temp = n * factorial(n - 1);
	return temp;
}

int main()
{
	int n, temp;
	cin >> n;
	
	for (size_t i = 0; i < n; i++)
	{
		cout << setfill(' ') << setw((n * 2 + 3) - i * 2);
		for (size_t j = 0; j <= i; j++)
		{
			
			if (j == i)
			{
				temp = factorial(i) / (factorial(i - j) * factorial(j));
				cout << temp << endl;	
			}
			else
			{
				temp = factorial(i) / (factorial(i - j) * factorial(j));
				cout << temp << setfill(' ') << setw(4);
			}
		}
	}
    return 0;
}

