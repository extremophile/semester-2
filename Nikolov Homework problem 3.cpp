/***
FN:F79454
PID:3
GID:1
*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdlib>

using namespace std;

int main(int argc, char * argv[])
{
	if (argc > 3)
	{
		return -1;
	}
	ifstream input(argv[1]);
	ofstream output(argv[2]);
	if (!input.is_open() && !output.is_open())
	{
		return -1;
	}

	char c_str[256];
	while (input.getline(c_str, 256))
	{
		string str(c_str);
		for (int i = 0; i < str.size(); i++)
		{
			if (islower(str[i]) || isdigit(str[i]) || isspace(str[i]))
			{
				output << str[i];
			}
		}
		output << endl;
	}
	input.close();
	output.close();
}

