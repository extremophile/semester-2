#include <iostream>
#include <string>
#include <vector>
 
using namespace std;
 
class Person
{
private:
	string name;
	Person* bestFriend;
	int popularity;
 
public:
	Person(string n)
	{
		name = n;
		popularity = 0;
	}
 
	void increasePopularity()
	{
		popularity++;
	}
 
	string getName()
	{
		return name;
	}
	void setBestFriend(Person * u)
	{
		bestFriend = u;
		u->increasePopularity();
	}
 
	int getPopularity()
	{
		return popularity;
	}
 
};
 
int main()
{
	vector<Person*>p;
 
	int n;
	cin >> n;
 
	for (int i = 0; i < n; i++)
	{
		string s;
		cin >> s;
		p.push_back(new Person(s));
	}
	for (int i = 0; i < n; i++)
	{
		string q;
		cin >> q;
 
		bool isExistent = false;
		for (int j= 0; j < p.size(); j++)
		{
			if ((*p[j]).getName() == q) // p[j]->getName() != q <<< alternative way (right)
			{
				p[i]->setBestFriend(p[j]);
				isExistent = true;
				break;
			}
		}
		if (!isExistent) {
			cout << "Person " << q << " is not recognized!" << endl;
		}
	}
	for (int i = 0; i < p.size(); i++)
	{
		cout << p[i]->getName() << " " << p[i]->getPopularity() << endl;
	}
 
    return 0;
}
