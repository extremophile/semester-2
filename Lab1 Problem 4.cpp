// problem 4 another try.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>

using namespace std;

vector<int> merge(vector<int> first, vector<int> second) {
	vector<int>result;

	for (int i = 0; i<first.size() && i<second.size(); i++) 
	{
		if (first[i] > second[i]) 
		{
			result.push_back(second[i]);
			result.push_back(first[i]);
		}
		else 
		{
			result.push_back(first[i]);
			result.push_back(second[i]);
		}
	}

	if (first.size() > second.size()) 
	{

		for (int i = second.size(); i < first.size(); i++)
		{
			result.push_back(first[i]);
		}
	}
	else if (second.size() > first.size()) 
	{

		for (int i = first.size(); i < second.size(); i++)
		{
			result.push_back(second[i]);
		}
	}
	return result;
}

int main()
{
	int n, m;
	cout << "Enter how many elements the first vector has:" << endl;
	cin >> n;
	vector<int> first(n);
	for (int i = 0; i < n; i++)
	{
		cin >> first[i];
	}
	
	cout << "Enter how many elements the second vector has:" << endl;
	cin >> m;
	vector<int> second(m);
	for (int i = 0; i < m; i++)
	{
		cin >> second[i];
	}
	vector<int> res = merge(first, second);

	for (int i = 0; i < res.size(); i++)
		cout << res[i] << " ";

    return 0;
}

