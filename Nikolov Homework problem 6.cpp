/***
FN:F79454
PID:6
GID:1
*/

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

using namespace std;

void countWord(string a, string b)
{
	ifstream input(a.c_str());
	if (!input.is_open())
	{
		return;
	}
	string c;
	int counter = 0;
	while (input >> c)
	{
		c.erase(remove_if(c.begin(), c.end(), ispunct), c.end());
		for (int i = 0; i < c.length(); i++)
		{
			c[i] = tolower(c[i]);
		}
		if (c == b)
		{
			counter++;
		}
	}
	input.close();
	if (counter == 0)
	{
		return;
	}
	else
	{
		cout << a << " " << counter << endl;
	}
}

int main(int argc, char * argv[])
{
	string a;
	for (size_t i = 2; i < argc; i++)
	{
		countWord(argv[i], argv[1]);
	}

	return 0;
}
