/***
FN:F79454
PID:2
GID:1
*/

#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

int main(int argc, char * argv[])
{
	if (argc > 5)
	{
		return -1;
	}
	ifstream input(argv[1]);
	ofstream output(argv[2]);
	if (!input.is_open() && !output.is_open())
	{
		return -1;
	}
	int number;
	while (input >> number)
	{
		if (argv[3][0] == '+') // na 3tiq string purviq
		{
			int newNumber = atoi(argv[4]) + number; //atoi prevrushta string v int
			output << newNumber << " ";
		}
		else if (argv[3][0] == '-')
		{
			int newNumber = number - atoi(argv[4]);
			output << newNumber << " ";
		}
		else if (argv[3][0] == 'x')
		{
			int newNumber = number*atoi(argv[4]);
			output << newNumber << " ";
		}
	}
	input.close();
	output.close();
}
