/***
FN:F79454
PID:5
GID:1
*/

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
using namespace std;

int iMin;

void printVector(vector<int>& A)
{
	for (int i = 0; i < A.size(); i++)
		cout << A[i] << (i == A.size() - 1 ? "\n" : " ");
}

void bubbleSort(vector<int>& A)
{
	bool swapped = true;
	for (size_t i = 0; i < A.size() - 1 && swapped;)
	{
		swapped = false;
		for (size_t j = i; j < A.size() - 1; j++)
		{
			if (A[j] > A[j + 1])
			{
				swap(A[j], A[j + 1]);
				swapped = true;
			}
		}
		printVector(A);
	}
}

void selectionSort(vector<int>& A)
{
	int i, j, minIndex, tmp;
	for (i = 0; i < A.size() - 1; i++)
	{
		minIndex = i;
		for (j = i; j < A.size(); j++)
		{
			if (A[j] < A[minIndex])
			{
				minIndex = j;
			}
		}
		if (minIndex != i)
		{
			swap(A[minIndex], A[i]);
		}
		printVector(A);
	}
}

int main()
{
	int n;
	cin >> n;
	int number;
	vector<int> A(n);
	for (size_t i = 0; i < n; i++)
	{
		cin >> number;
		A[i] = number;
	}

	vector<int> bsCopy(A.begin(), A.end());
	bubbleSort(bsCopy);

	cout << endl;

	vector<int> ssCopy(A.begin(), A.end());
	selectionSort(ssCopy);

	return 0;
}
