#include <iostream>
 
using namespace std;
 
int main()
{
	cout << "Enter a number:" << endl;
	int a;
	cin >> a;
	int *pa = &a;
 
	cout << "Number: " << a << endl;
	cout << "Pointer: " << pa << endl;
 
    return 0;
}
