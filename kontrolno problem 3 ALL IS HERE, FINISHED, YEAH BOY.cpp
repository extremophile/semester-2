#include <iostream>
#include <string>
#include <stdexcept>
#include <sstream>
#include <algorithm>
#include <fstream>
using namespace std;
 
template<typename T>
class List
{
public:
	List()
	{
		capacity = 1;
		count = 0;
		elements = new T[1];
	}
	List(const List<T> &other)
	{
		count = other.count;
		capacity = other.capacity;
		elements = new T[other.capacity];
		for (int i = 0; i < other.count; i++)
		{
			elements[i] = other.elements[i];
		}
	}
	List<T>& operator=(const List<T> & right)
	{
		if (this != &right)
		{
			if (capacity < right.capacity)
			{
				T * arr = new T[right.capacity];
				for (int i = 0; i < right.count; i++)
				{
					arr[i] = right.elements[i];
				}
				delete[] elements;
				elements = arr;
				count = right.count;
				capacity = right.capacity;
			}
			else
			{
				for (int i = 0; i < right.count; i++)
				{
					elements[i] = right.elements[i];
				}
				count = right.count;
			}
		}
		return *this;
	}
 
	~List()
	{
		delete[] elements;
	}
 
	T get(int i) const
	{
		return elements[i];
	}
 
	void add(T data)
	{
		if (this->count == this->capacity)
		{
			resize(capacity + 1);
			elements[count] = data;
			count++;
		}
		else
		{
			elements[count] = data;
			count++;
		}
	}
	T pop() /// shtoto go ignoriram
	{
		--count;
		return elements[count];
	}
	int getCount() const
	{
		return count;
	}
 
	T& operator[](unsigned int index)
	{
		if (index >= count)
		{
			throw out_of_range("Out of range");
		}
		return elements[index];
	}
	const T& operator[](unsigned int index) const
	{
		if (index >= count)
		{
			throw out_of_range("Out of range");
		}
		return elements[index];
	}
 
	List<T> operator+(List<T> &right)
	{
		int maxCapacity = max(this->capacity, right.capacity);
		List<T> * arr = new List(); // ne e hubavo da se allokirat konteineri na steka ili neshta koito imat 
		arr->resize(maxCapacity);
		//T * arr = new T[maxCapacity];
		int minCount = min(this->count, right.count);
		for (int i = 0; i < minCount; i++)
		{
			arr->add(this->elements[i] + right.elements[i]);
		}
		if (this->count > right.count)
		{
			for (int i = right.count; i < maxCapacity; i++)
			{
				arr->add(this->elements[i]);
			}
		}
		else
		{
			for (int i = this->count; i < maxCapacity; i++)
			{
				arr->add(right.elements[i]);
			}
		}
 
		return *arr;
	}
 
private:
	T * elements;
	int capacity;
	int count;
 
	//use this function to allocate a new, bigger array of elements to fit the added values. Don't forget to delete the old array
	void resize(int newCapacity)
	{
		T * arr = new T[newCapacity];
		for (int i = 0; i < count; i++)
		{
			arr[i] = elements[i];
		}
		capacity = newCapacity;
		delete[] elements;
		elements = arr;
	}
};
 
template<typename T>
ostream& operator<<(ostream &os, const List<T> &arr) ///poneje e po referenciq ne polzva copy constructora
{
	os << "(";
	for (int i = 0; i < arr.getCount(); i++)
	{
		if (i == arr.getCount() - 1)
		{
			os << arr[i] << ")";
		}
		else
		{
			os << arr[i] << ",";
		}
	}
	return os;
}
 
template<typename T>
istream& operator>>(istream &is, List<T> &arr)
{
	string line;
	getline(is, line);
	stringstream s(line);
	T tmp;
	while (s >> tmp)
	{
		arr.add(tmp);
	}
	return is;
}
 
template<typename T>
void print(List<T> list) // ako beshe s & nqmashe da polzva copy constuctor
{
	for (int i = 0; i < list.getCount(); i++)
		cout << list.get(i) << " ";
	cout << endl;
}
 
template<typename T>
void print2(const List<T> &list)
{
	for (int i = 0; i < list.getCount(); i++)
		cout << list[i] << " ";
	cout << endl;
}
 
int main(int argc, char* argv[])
{
	List<float> list1;
	list1.add(1.0f);
	list1.add(2.0f);
	list1.add(3.0f);
	cout << list1.get(0) << " " << list1.get(1) << " " << list1.get(2) << endl;
 
	List<float> list2;
	list2 = list1;
	cout << list2.get(0) << " " << list2.get(1) << " " << list2.get(2) << endl;
 
	list2.add(4.0f);
	cout << list2.get(0) << " " << list2.get(1) << " " << list2.get(2) << " " << list2.get(3) << endl;
	cout << list1.pop() << endl;
	cout << list1.getCount() << endl;
 
	print(list1);
	print(list1);
	print(list2);
 
	cout << "--" << endl;
	print2(list1);
 
	//worked till here
 
	cout << list1 << endl;
	cout << list2 << endl;
 
	cout << list1[1] << endl;
	list2[2] += 10;
	cout << list2[2] << endl;
 
	ifstream input("3.in");
	List<string> list4;
	input >> list4;
	cout << list4 << endl;
 
	cout << "--" << endl;
 
	List<float> list3 = list1 + list2;
	cout << list1 << endl;
	cout << list2 << endl;
	cout << list3 << endl;
 
	try
	{
		cout << list2[4] << "!" << endl;
	}
	catch (out_of_range &e)
	{
		cout << "Out of range exception was thrown." << endl;
	}
 
	return 0;
 
}
