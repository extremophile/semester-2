/***
FN:F79454
PID:4
GID:1
*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <vector>

using namespace std;

long long iterative(int n)
{

	if (n < 4)
	{
		return 1;
	}
	long long f1 = 1;
	long long f2 = 1;
	long long f3 = 1;
	long long sum;
	for (size_t i = 3; i < n; i++)
	{
		sum = 3 * f3 - 2 * f2 + f1;
		f1 = f2;
		f2 = f3;
		f3 = sum;
	}
	return sum;
}

long long recursive(int n)
{
	if (n < 4)
	{
		return 1;
	}
	else
	{
		return 3 * recursive(n - 1) - 2 * recursive(n - 2) + recursive(n - 3);
	}
}

int main(int argc, char * argv[])
{
	if (argc > 2)
	{
		return -1;
	}
	int n;
	n = atoi(argv[1]);
	cout << "Recursive: " << recursive(n) << endl;
	cout << "Iterative: " << iterative(n) << endl;
	return 0;
}

