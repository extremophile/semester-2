// problem 2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

void Ins(vector<double> &a)
{
	double temp;
	for (size_t i = 1; i < a.size(); i++)
	{
		temp = a[i];
		if (a[i - 1] > a[i])
		{
			for (int j = i; j > 0; j--)
			{
				if (temp < a[j - 1])
				{
					if (j == 1 && temp < a[0])
					{
						a[1] = a[0];
						a[0] = temp;
						break;
					}
					else
					{
						a[j] = a[j - 1];
					}	
				}
				else
				{
					a[j] = temp;
					break;
				}
			}
		}
	}
}
/**
int i, j;
double tmp;

for (i = 1; i < a.size(); i++)
{
	j = i;
	while (j > 0 && a[j - 1] > a[j])
	{
		tmp = a[j];
		a[j] = a[j - 1];
		a[j - 1] = tmp;
		j--;
	}
}*/

int main(int argc, char * argv[])
{
	vector<double> vec;
	double n;
	ifstream input;
	for (int i = 1; i < argc; i++)
	{
		input.open(argv[i]);
		if (!input.is_open())
		{
			continue;
		}
		else
		{
			for (size_t i = 0; input >> n; i++)
			{
				vec.push_back(n);
			}
		}
		input.close();
	}

	Ins(vec);

	for (size_t i = 0; i < vec.size(); i++)
	{
		cout << vec[i] << endl;
	}
	return 0;
}

