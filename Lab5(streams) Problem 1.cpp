// problem1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int return_char(istream &in)
{
	char n;
	int count = 0;
	in.get();
	while (!in.eof())
	{
		count++;
		in.get();
	}
	return count;
}

int return_words(istream &in)
{
	string n;
	int count = 0;
	while (in >> n)
	{
		count++;
	}
	return count;
}

int return_lines(istream &in)
{
	int count = 0;
	string n;
	while (getline(in, n))
	{
		count++;
	}
	return count;
}

int main()
{
	ifstream input;
	input.open("1.in");
	if (input.fail())
	{
		cout << "Error, you are retarded." << endl;
		return -1;
	}
	cout << "chars: " << return_char(input) << endl;
	input.clear();
	input.seekg(0, input.beg);
	cout << "words: " << return_words(input) << endl;
	input.clear();
	input.seekg(0, input.beg);
	cout << "lines: " << return_lines(input) << endl;
	input.close();
	return 0;
}

