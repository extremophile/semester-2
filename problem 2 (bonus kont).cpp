// problem 2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <stdexcept> 
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

template<typename T>
class Vec3
{
public:
	Vec3()
	{
		components = new T[3];
		size = 3;
	}
	Vec3(const Vec3<T> &other) : Vec3()
	{
		for (int i = 0; i < other.size; i++)
		{
			components[i] = other.components[i];
		}
	}

	Vec3<T>& operator=(const Vec3<T> &right)
	{
		if (this != &right)
		{

			T * temp = new T[right.size];
			for (int i = 0; i < right.size; i++)
			{
				temp[i] = right.components[i];
			}
			delete[] components;
			components = temp;
			size = right.size;
		}
		return *this;
	}

	~Vec3()
	{
		delete[] components;
	}

	T& operator[](unsigned int index)
	{
		return components[index];
	}
	T& operator[](unsigned int index) const
	{
		return components[index];
	}

	int get_size() const
	{
		return size;
	}
private:
	T *components;
	unsigned int size;
};

template<typename T>
ostream& operator<<(ostream& os, const Vec3<T>& v)
{
	os << "(";
	for (int i = 0; i < v.get_size(); i++) //
	{
		if (i == v.get_size() - 1)
		{
			os << v[i] << ")";
		}
		else
		{
			os << v[i] << ", ";
		}
	}
	return os;
}

template<typename T>
istream& operator>>(istream& is, const Vec3<T>& v) // ���� ������� ���� �� 3 ����� ����� ��� � ���� ������ ������ �� �������� ���� � ���� ������
												   // �� ������ ������� � stringstream �� ss �� �������� �������� � �� �� ������� � ������ �� ������
{
	string line;
	getline(is, line);
	stringstream sStream(line);


	for (int i = 0; i < v.get_size(); i++)
	{
		sStream >> v[i];
	}
	return is;
}

int main(int argc, char* argv[])
{
	vector< Vec3<float> > vs;
	Vec3<float> temp;
	ifstream input("2.in");
	while (input >> temp)
	{
		vs.push_back(temp);
	}

	cout << "vs = ";
	for (int i = 0; i < vs.size(); i++)
	{
		cout << vs[i] << ", ";
	}
	cout << endl;

	return 0;
}