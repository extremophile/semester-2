// problem1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Person
{
protected:
	string name;
	string birthDate;

public:
	Person(string name_, string birthDate_) : name(name_), birthDate(birthDate_)
	{

	}
	virtual void print()
	{
		cout << name << " " << birthDate << endl;
	}
};

class Student : public Person
{
protected:
	string prog;
public:
	Student(string name_, string birthDate_, string program) : Person(name_, birthDate_), prog(program)
	{
	}
	virtual void print()
	{
		Person::print();
		cout << "Studies " << prog << endl;
	}

};

class Instructor : public Person
{
protected:
	double salary;
public:
	Instructor(string name_, string birthDate_, double sal) : Person(name_, birthDate_), salary(sal)
	{
	}
	virtual void print()
	{
		Person::print();
		cout << "Earns " << salary << endl;

	}
};

int main()
{
	int n;
	cin >> n;
	vector<Person*> v;
	while (n > 0)
	{
		char type;
		cin >> type;
		if (type == 's')
		{
			string name, birthDate, program;
			cin >> name >> birthDate >> program;
			Student * n = new Student(name, birthDate, program);
			v.push_back(n);
		}
		else
		{
			string name, birthDate;
			double salary;
			cin >> name >> birthDate >> salary;
			Instructor * n = new Instructor(name, birthDate, salary);
			v.push_back(n);
		}
		n--;
	}
	int number;

	while (cin >> number)
	{
		v[number - 1]->print();
		cout << endl;
	}
}
