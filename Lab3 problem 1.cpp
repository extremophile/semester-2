#include <iostream>
#include <string>
#include <algorithm>


using namespace std;

class Weapon
{
public:
	Weapon(double a)
	{
		damageModifier = a;
	}
	Weapon()
	{
		damageModifier = 0;
		cout << ".. That's a weak ass weapon ;//" << endl;
	}

	double getModifier() const
	{
		return damageModifier;
	}
	~Weapon()
	{

	}

private:
	double damageModifier;
};

class Character
{
public:
	Character()
	{
		health = 0;
		name = "Mr. Nobody";
		attackPower = 0;
	}
	Character(int health, string name, int attackPower) : health(health), name(name), attackPower(attackPower)
	{
	}
	~Character()
	{
	}

	double getHealth() const
	{
		return health;
	}
	string getName() const
	{
		return name;
	}
	void setHealth(const int &health)
	{
		this->health = max(health, 0); 
		if (health <= 0)
		{
			die();
		}
	}

	void die()
	{
		health = 0;
		cout << "Maika mu da ibaaa... " << name << " umre." << endl;
	}

	double calculateDamage()
	{
		return attackPower*weapon->getModifier();
	}

	void reduceHealth(int damageTaken)
	{
		health -= damageTaken; 
		setHealth(health);
	}

	void attack(Character* other)
	{
		other->reduceHealth(this->calculateDamage());  
	}

	void setWeapon(const Weapon * weapon_)
	{
		weapon = weapon_;
	}

private:
	string name;
	int health;
	int attackPower;
	const Weapon* weapon;
};


int main()
{
	Character gimli(20, "Gimli", 5);
	Weapon *axe = new Weapon(3); 
	gimli.setWeapon(axe);

	Character orc(10, "Meto", 2);
	gimli.attack(&orc); 

	return 0;
}
