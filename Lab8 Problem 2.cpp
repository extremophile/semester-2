// problem 2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <set>
using namespace std;

int main(int argc, char* argv[])
{
	if (argc > 2)
	{
		return 0;
	}
	ifstream input;
	input.open(argv[1]);

	multiset<string> m;
	multiset<string>::iterator it;
	multiset<string>::iterator it2;

	string word;
	while (input >> word)
	{
		m.insert(word);
	}

	for (it = m.begin(); it != m.end(); it++)
	{
		if (m.count(*it) > 1)
		{
			cout << *it << ": " << m.count(*it) << endl;
			it++;
		}
		else
		{
			cout << *it << ": " << m.count(*it) << endl;
		}
	}
    return 0;
}

