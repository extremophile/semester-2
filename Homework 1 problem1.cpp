/***
FN:F79454
PID:1
GID:3
*/

#include <iostream>
#include <string>

using namespace std;

//implement class Employee
//implement method printEmployeeInfo
class Employee
{
public:
	Employee()
	{
	}
	Employee(string name_, double salary_) : name(name_), salary(salary_)
	{
	}
	~Employee() {}


	void print()
	{
		cout << "My name is " << getName() << " and I earn " << salary << "BGN." << endl;
	}

	void setName(string a)
	{
		name = a;
	}

	void setSalary(double b)
	{
		salary = b;
	}

	string getName() const
	{
		return name;
	}

	double getSalary() const
	{
		return salary;
	}

private:
	string name;
	double salary;
};


void printEmployeeInfo(Employee * p)
{

	cout << "The name of the employee is " << p->getName() << "." << endl;
	cout << p->getName() <<" earns " << p->getSalary() << "BGN." << endl;
}


int main()
{
	Employee* employee = new Employee("John", 800);

	/*
	Should print:
	The name of the employee is John.
	John earns 800BGN.
	*/
	printEmployeeInfo(employee);

	//Should print "My name is John and I earn 800BGN."
	employee->print();

	employee->setName("Tomas");
	employee->setSalary(1500);

	/*
	Should print:
	The name of the employee is Tomas.
	Tomas earns 1500BGN.
	*/
	printEmployeeInfo(employee);

	//Should print "My name is Tomas and I earn 1500BGN."
	employee->print();

	delete employee;
	return 0;
}
