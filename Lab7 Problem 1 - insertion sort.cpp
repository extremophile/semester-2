// insertion sort.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

int main()
{
	int a[] = { 1, 4, 3, 5, 6, 2 };
	int n = 6;
	int temp;

	for (size_t i = 1; i < n; i++)
	{
		temp = a[i];
		if (a[i] < a[i - 1])
		{
			for (int j = i; j > 0; j--)
			{
				if (a[j - 1] > temp)
				{
					a[j] = a[j - 1];
				}
				else
				{
					a[j] = temp;
				}
			}
		}	
	}
	for (size_t i = 0; i < n; i++)
	{
		if (i == n - 1)
		{
			cout << a[i] << endl;
		}
		else
		{
			cout << a[i] << " ";
		}
	}
    return 0;
}

