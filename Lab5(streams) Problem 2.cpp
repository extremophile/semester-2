// problem 2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iomanip>

using namespace std;

class Student
{
public:
	Student(string facultyNumber_, string firstName_, string lastName_, double avgGrade_) : facultyNumber(facultyNumber_), firstName(firstName_), lastName(lastName_), avgGrade(avgGrade_)
	{}

	void print()
	{
		cout << facultyNumber << setw(11) << firstName << setw(16) << lastName << setw(10) << setprecision(3) << avgGrade << endl;
	}

	~Student() {}

private:
	string facultyNumber;
	string firstName;
	string lastName;
	double avgGrade;
};

int main()
{
	ifstream input;
	input.open("2.in");
	vector<Student *> v;
	string facultyNumber, firstName, lastName;
	int numberOfGrades;

	while (input >> facultyNumber)
	{
		if (facultyNumber[0] != 'f')
		{
			break;
		}
		input >> firstName >> lastName >> numberOfGrades;
		double grade;
		double avgGrade = 0;
		for (int i = 0; i < numberOfGrades; i++)
		{
			input >> grade;
			avgGrade += grade;
		}
		double afterCalculation = avgGrade / numberOfGrades;
		Student * student = new Student(facultyNumber, firstName, lastName, afterCalculation);
		v.push_back(student);
	}

	cout << "Fac. No" << setw(15) << "First Name" << setw(13) << "Last Name" << setw(8) << "Avg" << endl;

	for (int j = 0; j < v.size(); j++)
	{
		v[j]->print();
	}
	input.close();

    return 0;
}

