// problem 1.cpp : Defines the entry point for the console application.
//

/***
FN:F00000
PID:1
GID:1
*/
#include "stdafx.h"
#include <iostream>
#include <vector>
#include <stdexcept>

using namespace std;

template<typename T>
class Vec3
{
public:
	Vec3()
	{
		components = new T[3];
		size = 3;
	}
	Vec3(const Vec3<T> &other) : Vec3() // vika default constructora v toq sluchai
	{
		for (int i = 0; i < other.size; i++)
		{
			components[i] = other.components[i];
		}
	}
	Vec3<T>& operator=(const Vec3<T> &right)
	{
		if (this != &right)
		{

			T * temp = new T[right.size];
			for (int i = 0; i < right.size; i++)
			{
				temp[i] = right.components[i];
			}
			delete[] components;
			components = temp;
			size = right.size;
		}
		return *this;
	}

	~Vec3()
	{
		delete[] components;
	}

	T get(int i) const
	{
		return components[i];
	}
	void set(int i, T val)
	{
		components[i] = val;
	}
private:
	T *components;
	unsigned int size;
};

int main(int argc, char* argv[])
{
	vector< Vec3<float> > vs;
	Vec3<float> temp;
	float a, b, c;
	cin >> a >> b >> c;
	temp.set(0, a);
	temp.set(1, b);
	temp.set(2, c);

	vs.push_back(Vec3<float>(temp));

	temp.set(0, 5.0f);
	cout << "temp.get(0)=" << temp.get(0) << endl;
	cout << "vs[0].get(0)=" << vs[0].get(0) << endl;

	vs[0] = temp;
	cout << "temp.get(0)=" << temp.get(0) << endl;
	cout << "vs[0].get(0)=" << vs[0].get(0) << endl;

	temp.set(0, 1.0f);
	cout << "temp.get(0)=" << temp.get(0) << endl;
	cout << "vs[0].get(0)=" << vs[0].get(0) << endl;

	return 0;
}