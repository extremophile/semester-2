// Problem 1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

bool compute(int a)
{
	if (a % 4 == 0 && a % 100 != 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int main()
{
	cout << "Enter a year:" << endl;
	int year;
	while (cin >> year)
	{
		if (compute(year))
		{
			cout << "YES" << endl;
		}
		else
		{
			cout << "NO" << endl;
		}
	}
    return 0;
}

