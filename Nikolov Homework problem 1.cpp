/***
FN:F79454
PID:1
GID:1
*/

#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

int main(int argc, char * argv[])
{
	if (argc > 2)
	{
		return -1;
	}
	ifstream input;
	input.open(argv[1]);
	if (!input.is_open())
	{
		return -1;
	}
	int number, minn, maxx, sum = 0;
	int counter = 0;
	if (input >> number)
	{
		minn = number;
		maxx = number;
		sum += number;
		counter++;
	}

	while (input >> number)
	{
		if (number < minn)
		{
			minn = number;
		}
		if (number > maxx)
		{
			maxx = number;
		}
		sum += number;
		counter++;
	}
	cout << "SUM: " << sum << endl;
	cout << "AVG: " << setprecision(1) << fixed << (double)sum / counter << endl;
	cout << "MIN: " << minn << endl;
	cout << "MAX: " << maxx << endl;
	input.close();
	return 0;
}
